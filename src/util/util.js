import auth from '@react-native-firebase/auth';

export const HandleCreateAccountFirebase = (email, password) => {
  return auth().createUserWithEmailAndPassword(email, password);
};

export const HandleSignInFirebase = (email, password) => {
  return auth().signInWithEmailAndPassword(email, password);
};

export const HandleSignOutFirebase = () => {
  return auth().signOut();
};

// export const HandleSignInGoogle = async () => {
//   // Check if your device supports Google Play
//   await GoogleSignin.hasPlayServices({showPlayServicesUpdateDialog: true});
//   // Get the users ID token
//   const {idToken} = await GoogleSignin.signIn();

//   // Create a Google credential with the token
//   const googleCredential = auth.GoogleAuthProvider.credential(idToken);

//   // Sign-in the user with the credential
//   return auth().signInWithCredential(googleCredential);
// };
