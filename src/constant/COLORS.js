export default COLOUR = {
  white: '#FFFFFF',
  black: '#000000',
  lightText: '#00000090',
  onBoardCardBG: '#EBC8FA',
  bgLineGradOne: '#FFFEFE',
  bgLineGradTwo: '#F6F5FC',
  bgLineGradThree: '#EEEBF9',
  bgLineGradFour: '#E3E6F6',
  bgLineGradFive: '#DCE2EC',
  bgLineGradSix: '#E5DEE4',
  transparent: 'transparent',
  warning: '#F88070',
  accent: '#FF6060',
  primary: '#FF7E92',
  iconSelected: '#fe5e00',
};
