import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import COLORS from '../constant/COLORS';

const TextInputContainer = ({
  title,
  state,
  setState,
  type,
  secure,
  isSecure,
  setIsSecure,
  isPassword,
  errorsText,
  clear,
}) => {
  return (
    <View style={{paddingHorizontal: 25, marginBottom: 10}}>
      <Text>{title}</Text>
      <View>
        <TextInput
          style={{
            height: 50,
            backgroundColor: COLORS.white,
            paddingVertical: 10,
            paddingHorizontal: 20,
            fontSize: 14,
            borderRadius: 10,
            color: COLORS.black,
            marginTop: 10,
          }}
          onChangeText={text => setState(text)}
          value={state}
          placeholder={`Input your ${title}`}
          placeholderTextColor={COLORS.lightText}
          keyboardType={type}
          secureTextEntry={isSecure}
          onFocus={() => clear()}
        />
        {isPassword && (
          <Icon
            name={isSecure ? 'eye-off' : 'eye'}
            size={28}
            color={COLORS.black}
            onPress={() => setIsSecure(!isSecure)}
            style={{position: 'absolute', right: 10, top: 20}}
          />
        )}
        {errorsText && (
          <Text
            style={{
              fontSize: 14,
              color: COLORS.warning,
              marginTop: 4,
            }}>
            {errorsText}
          </Text>
        )}
      </View>
    </View>
  );
};

export default TextInputContainer;

const styles = StyleSheet.create({});
