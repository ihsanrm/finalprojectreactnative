import auth from '@react-native-firebase/auth';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import COLORS from '../constant/COLORS';
import Cart from '../screen/appScreens/Cart/Cart';
import DetailProduct from '../screen/appScreens/DetailProduct/DetailProduct';
import Home from '../screen/appScreens/Home/Home';
import Product from '../screen/appScreens/Product/Product';
import Profile from '../screen/appScreens/Profile/Profile';
import SignIn from '../screen/auth/SignIn/SignIn';
import SignUp from '../screen/auth/SignUp/SignUp';
import Splash from '../screen/auth/Splash/Splash';

const Router = () => {
  const Stack = createNativeStackNavigator();

  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  // Handle user state changes
  const onAuthStateChanged = user => {
    setUser(user);
    if (initializing) setInitializing(false);
  };

  const MyTabBar = ({state, descriptors, navigation}) => {
    return (
      <View style={{flexDirection: 'row'}}>
        {state.routes.map((route, index) => {
          const {options} = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          const HandleIcon = title => {
            if (title === 'Home') {
              return 'home';
            } else if (title === 'Cart') {
              return 'cart';
            } else if (title === 'Product') {
              return 'shirt';
            } else if (title === 'Profile') {
              return 'person';
            }
          };

          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{
                flex: 1,
                alignItems: 'center',
                paddingTop: 10,
                backgroundColor: COLORS.white,
                borderTopColor: COLORS.black,
                borderTopWidth: 0.5,
              }}>
              <Icon
                name={HandleIcon(label)}
                size={25}
                color={isFocused ? COLORS.iconSelected : COLORS.black}
              />
              <Text
                style={{color: isFocused ? COLORS.iconSelected : COLORS.black}}>
                {label}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  const MyTabs = () => {
    const Tab = createBottomTabNavigator();

    return (
      <Tab.Navigator
        screenOptions={{headerShown: false}}
        tabBar={props => <MyTabBar {...props} />}>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Product" component={Product} />
        <Tab.Screen name="Cart" component={Cart} />
        <Tab.Screen name="Profile" component={Profile} />
      </Tab.Navigator>
    );
  };

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        {!user ? (
          <>
            <Stack.Screen name="Splash" component={Splash} />
            <Stack.Screen name="SignIn" component={SignIn} />
            <Stack.Screen name="SignUp" component={SignUp} />
          </>
        ) : (
          <>
            <Stack.Screen name="Home" component={MyTabs} />
            <Stack.Screen name="DetailProduct" component={DetailProduct} />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Router;

const styles = StyleSheet.create({});
