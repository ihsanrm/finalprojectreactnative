import React, {useState} from 'react';
import {
  Image,
  Keyboard,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import TextInputContainer from '../../../components/TextInputContainer';
import COLORS from '../../../constant/COLORS';
import {HandleSignInFirebase} from '../../../util/util';

const SignIn = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isPasswordSecure, setIsPasswordSecure] = useState(true);

  const [showErrors, setShowErrors] = useState(false);
  const [errors, setErrors] = useState({});

  const getErrors = (email, password) => {
    Keyboard.dismiss();
    const errors = {};

    if (!email) {
      errors.email = 'Please Input Your Email';
    } else if (!email.includes('@') && !email.includes('.com')) {
      errors.email = 'Please Input Valid Email';
    }

    if (!password) {
      errors.password = 'Please Input Your Password';
    } else if (password.length < 8) {
      errors.password = 'Please Input Password of 8 Characters';
    }

    return errors;
  };

  const HandleRegister = () => {
    const errors = getErrors(email, password);

    if (Object.keys(errors).length > 0) {
      setShowErrors(true);
      setErrors(showErrors && errors);
      console.log(errors);
    } else {
      setErrors({});
      setShowErrors(false);
      console.log(email);
      console.log(password);
      console.log('register');
      HandleSignIn(email, password);
    }
  };

  const HandleSignIn = (email, password) => {
    HandleSignInFirebase(email, password)
      .then(() => {
        ToastAndroid.show('Account Created', ToastAndroid.SHORT);
      })
      .catch(error => {
        console.log(error.code);
        if (error.code === 'auth/user-not-found') {
          return setErrors({email: 'Email not found'});
        } else if (error.code === 'auth/wrong-password') {
          return setErrors({password: 'Password is invalid'});
        }
        setShowErrors(false);
        setErrors({});
      });
  };

  const Slogan = () => {
    return (
      <Text style={{fontSize: 16, color: COLORS.black, fontWeight: '500'}}>
        Unlimited Shopping Center
      </Text>
    );
  };

  const Logo = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          width: '50%',
        }}>
        <View style={{}}>
          <Image
            source={require('../../../assets/logo/Logo.png')}
            style={{
              width: '100%',
              aspectRatio: 1 / 1,
            }}
          />
        </View>
        <View style={{flex: 1, marginLeft: 10}}>
          <Text style={{fontSize: 30, color: COLORS.black, fontWeight: 'bold'}}>
            GoShop
          </Text>
        </View>
      </View>
    );
  };

  const Header = () => {
    return (
      <View
        style={{
          height: '20%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Logo />
        <Slogan />
      </View>
    );
  };

  const SignUpMethod = () => {
    return (
      <View
        style={{
          flex: 0.1,
          justifyContent: 'center',
          marginTop: 20,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
          }}>
          <View>
            <Text
              style={{
                fontSize: 14,
                fontWeight: 'bold',
                color: COLORS.black,
                marginLeft: 5,
              }}>
              Dont Have An Account ?
            </Text>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
            <Text
              style={{
                fontSize: 14,
                fontWeight: 'bold',
                color: COLORS.iconSelected,
                marginLeft: 5,
              }}>
              SignUp Here
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const ButtonSignIn = () => {
    return (
      <TouchableOpacity
        onPress={() => HandleRegister()}
        style={{justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
        <View
          style={{
            height: 50,
            backgroundColor: COLORS.iconSelected,
            width: '80%',
            borderRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 18, fontWeight: '500s', color: COLORS.white}}>
            SignIn
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View>
      <LinearGradient
        colors={[
          COLORS.bgLineGradOne,
          COLORS.bgLineGradTwo,
          COLORS.bgLineGradThree,
          COLORS.bgLineGradFour,
          COLORS.bgLineGradFive,
          COLORS.bgLineGradSix,
        ]}
        style={{width: '100%', height: '100%'}}>
        <Header />
        <View style={{flex: 1, marginTop: 50}}>
          <View style={{flex: 1}}>
            <TextInputContainer
              title={'Email'}
              state={email}
              setState={setEmail}
              type={'email-address'}
              secure={false}
              isPassword={false}
              isSecure={isPasswordSecure}
              setIsSecure={setIsPasswordSecure}
              errorsText={errors.email}
              clear={() => setErrors({})}
            />
            <TextInputContainer
              title={'Password'}
              state={password}
              setState={setPassword}
              type={'text'}
              secure={true}
              isPassword={true}
              isSecure={isPasswordSecure}
              setIsSecure={setIsPasswordSecure}
              errorsText={errors.password}
              clear={() => setErrors({})}
            />
            <ButtonSignIn />
            <SignUpMethod />
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};

export default SignIn;
