import React, {useState} from 'react';
import {
  Image,
  Keyboard,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import TextInputContainer from '../../../components/TextInputContainer';
import COLORS from '../../../constant/COLORS';
import {HandleCreateAccountFirebase} from '../../../util/util';

const SignUp = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isPasswordSecure, setIsPasswordSecure] = useState(true);
  const [isConfirmPasswordSecure, setIsConfirmPasswordSecure] = useState(true);

  const [showErrors, setShowErrors] = useState(false);
  const [errors, setErrors] = useState({});

  const getErrors = (email, password, confirmPassword) => {
    Keyboard.dismiss();
    const errors = {};

    if (!email) {
      errors.email = 'Please Input Your Email';
    } else if (!email.includes('@') && !email.includes('.com')) {
      errors.email = 'Please Input Valid Email';
    }

    if (!password) {
      errors.password = 'Please Input Your Password';
    } else if (password.length < 8) {
      errors.password = 'Please Input Password of 8 Characters';
    }

    if (!confirmPassword) {
      errors.confirmPassword = 'Please Input Your Confirmation Password';
    } else if (confirmPassword.length < 8) {
      errors.confirmPassword = 'Please Input Correct Password';
    } else if (confirmPassword != password) {
      errors.confirmPassword = 'Password Not Matched';
    }

    return errors;
  };

  const HandleRegister = () => {
    const errors = getErrors(email, password, confirmPassword);

    if (Object.keys(errors).length > 0) {
      setShowErrors(true);
      setErrors(showErrors && errors);
      console.log(errors);
    } else {
      setErrors({});
      setShowErrors(false);
      console.log(email);
      console.log(password);
      console.log('register');
      HandleSignUp(email, password);
    }
  };

  const HandleSignUp = (email, password) => {
    HandleCreateAccountFirebase(email, password)
      .then(() => {
        ToastAndroid.show('Account Created', ToastAndroid.SHORT);
      })
      .catch(error => {
        console.log(error.code);
        if (error.code === 'auth/email-already-in-use') {
          return setErrors({email: 'Email already in use'});
        } else if (error.code === 'auth/invalid-email') {
          return setErrors({email: 'Email is invalid'});
        }
        setShowErrors(false);
        setErrors({});
      });
  };

  const Slogan = () => {
    return (
      <Text style={{fontSize: 16, color: COLORS.black, fontWeight: '500'}}>
        Unlimited Shopping Center
      </Text>
    );
  };

  const Logo = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          width: '50%',
        }}>
        <View style={{}}>
          <Image
            source={require('../../../assets/logo/Logo.png')}
            style={{
              width: '100%',
              aspectRatio: 1 / 1,
            }}
          />
        </View>
        <View style={{flex: 1, marginLeft: 10}}>
          <Text style={{fontSize: 30, color: COLORS.black, fontWeight: 'bold'}}>
            GoShop
          </Text>
        </View>
      </View>
    );
  };

  const Header = () => {
    return (
      <View
        style={{
          height: '20%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Logo />
        <Slogan />
      </View>
    );
  };

  const ButtonSignUpMethod = ({title, icons, onPress}) => {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          flexDirection: 'row',
          backgroundColor: COLORS.black,
          width: '45%',
          height: 50,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 10,
          marginHorizontal: 10,
        }}>
        <Icon name={icons} size={28} color={COLORS.white} onPress={() => {}} />
        <Text
          style={{
            marginLeft: 10,
            fontSize: 14,
            color: COLORS.white,
            fontWeight: '500',
          }}>
          SignIn {title}
        </Text>
      </TouchableOpacity>
    );
  };

  const SignUpMethod = () => {
    return (
      <View
        style={{
          flex: 0.3,
          justifyContent: 'center',
          marginTop: 20,
        }}>
        {/* <View
          style={{
            marginBottom: 25,
          }}>
          <View
            style={{
              height: 3,
              backgroundColor: COLORS.black,
              opacity: 0.2,
              width: '100%',
            }}>
            <Text>a</Text>
          </View>
        </View> */}

        {/* <ButtonSignUpMethod
            title={'Google'}
            icons={'logo-google'}
            onPress={() => HandleSignInGoogle()}
          /> */}
        {/* <ButtonSignUpMethod
            title={'Apple'}
            icons={'logo-apple'}
            onPress={() => HandleSignInApple()}
          /> */}

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
          }}>
          <View>
            <Text
              style={{
                fontSize: 14,
                fontWeight: 'bold',
                color: COLORS.black,
                marginLeft: 5,
              }}>
              Have An Account ?
            </Text>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('SignIn')}>
            <Text
              style={{
                fontSize: 14,
                fontWeight: 'bold',
                color: COLORS.iconSelected,
                marginLeft: 5,
              }}>
              SignIn Here
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const ButtonRegister = () => {
    return (
      <TouchableOpacity
        onPress={() => HandleRegister()}
        style={{justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
        <View
          style={{
            height: 50,
            backgroundColor: COLORS.iconSelected,
            width: '80%',
            borderRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 18, fontWeight: '500s', color: COLORS.white}}>
            Register
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View>
      <LinearGradient
        colors={[
          COLORS.bgLineGradOne,
          COLORS.bgLineGradTwo,
          COLORS.bgLineGradThree,
          COLORS.bgLineGradFour,
          COLORS.bgLineGradFive,
          COLORS.bgLineGradSix,
        ]}
        style={{width: '100%', height: '100%'}}>
        <Header />
        <View style={{flex: 1, marginTop: 50}}>
          <View style={{flex: 1}}>
            <TextInputContainer
              title={'Email'}
              state={email}
              setState={setEmail}
              type={'email-address'}
              secure={false}
              isPassword={false}
              isSecure={isPasswordSecure}
              setIsSecure={setIsPasswordSecure}
              errorsText={errors.email}
              clear={() => setErrors({})}
            />
            <TextInputContainer
              title={'Password'}
              state={password}
              setState={setPassword}
              type={'text'}
              secure={true}
              isPassword={true}
              isSecure={isPasswordSecure}
              setIsSecure={setIsPasswordSecure}
              errorsText={errors.password}
              clear={() => setErrors({})}
            />
            <TextInputContainer
              title={'Confirmation Password'}
              state={confirmPassword}
              setState={setConfirmPassword}
              type={'text'}
              secure={true}
              isPassword={true}
              isSecure={isConfirmPasswordSecure}
              setIsSecure={setIsConfirmPasswordSecure}
              errorsText={errors.confirmPassword}
              clear={() => setErrors({})}
            />
            <ButtonRegister />
            <SignUpMethod />
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};

export default SignUp;
