import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import COLORS from '../../../constant/COLORS';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('SignIn');
    }, 3000);
  }, []);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.white,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          width: '50%',
        }}>
        <View style={{}}>
          <Image
            source={require('../../../assets/logo/Logo.png')}
            style={{
              width: '100%',
              aspectRatio: 1 / 1,
            }}
          />
        </View>
        <View style={{flex: 1, marginLeft: 10}}>
          <Text style={{fontSize: 30, color: COLORS.black, fontWeight: 'bold'}}>
            GoShop
          </Text>
        </View>
      </View>
      <Text style={{fontSize: 16, color: COLORS.black, fontWeight: '500'}}>
        Unlimited Shopping Center
      </Text>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({});
