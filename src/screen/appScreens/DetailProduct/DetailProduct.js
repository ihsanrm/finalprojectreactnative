import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import COLORS from '../../../constant/COLORS';
import {addToCart, removeFromCart} from '../../../context/CartReducer';

const DetailProduct = ({navigation, route}) => {
  const [dataDetail, setDataDetail] = useState([]);
  const [dataRating, setDataRating] = useState([]);

  const cart = useSelector(state => state.cart.cart);
  const dispatch = useDispatch();
  console.log(cart);

  useEffect(() => {
    console.log('ini data');
    const data = route.params;
    console.log(data.data);
    console.log(data.data.rating.rate);
    setDataDetail(data.data);
    setDataRating(data.data.rating);
    console.log('INI CART NYA');
    console.log(cart);
  }, []);

  const addItemToCart = item => {
    dispatch(addToCart(item));
  };

  const removeItemFromCart = item => {
    dispatch(removeFromCart(item));
  };

  const Header = () => {
    return (
      <View
        style={{
          backgroundColor: COLORS.white,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,

          elevation: 5,
          paddingBottom: 20,
          borderBottomLeftRadius: 30,
          borderBottomRightRadius: 30,
        }}>
        <ImageBackground
          source={{
            uri: dataDetail.image,
          }}
          resizeMode="contain"
          style={{
            height: 300,
            width: '100%',
          }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{
              backgroundColor: COLORS.white,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,

              elevation: 5,
              width: 40,
              height: 40,
              borderRadius: 10,
              marginLeft: 10,
              marginTop: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon size={30} color={COLORS.black} name={'chevron-back'} />
          </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  };

  const TagProduct = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <View style={{width: 40}}>
          <Icon size={30} name={'pricetags'} color={COLORS.iconSelected} />
        </View>
        <View style={{flex: 1}}>
          <Text
            style={{
              fontSize: 14,
              textTransform: 'uppercase',
              color: COLORS.iconSelected,
              fontWeight: '500',
            }}>
            {dataDetail.category}
          </Text>
        </View>
      </View>
    );
  };

  const TitleProduct = () => {
    return (
      <View
        style={{
          marginBottom: 20,
          borderBottomColor: COLORS.lightText,
          borderBottomWidth: 0.5,
          paddingBottom: 20,
        }}>
        <View style={{flexDirection: 'row', marginBottom: 10}}>
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                color: COLORS.black,
                fontWeight: 'bold',
                letterSpacing: 1,
              }}>
              {dataDetail.title}
            </Text>
          </View>
          <TouchableOpacity style={{width: 40}}>
            <Icon
              size={40}
              name={'ellipsis-vertical-circle'}
              color={COLORS.iconSelected}
            />
          </TouchableOpacity>
        </View>
        <Text
          style={{
            fontSize: 14,
            fontWeight: '400',
            color: COLORS.lightText,
            letterSpacing: 0.5,
          }}>
          {dataDetail.description
            ? dataDetail.description.substring(0, 150)
            : '-'}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              flex: 0.4,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{width: 40}}>
              <Icon size={30} name={'star'} color={COLORS.iconSelected} />
            </View>
            <View style={{flex: 1, backgroundColor: COLORS.white}}>
              <Text
                style={{fontSize: 14, color: COLORS.black, fontWeight: 'bold'}}>
                {dataRating.rate} / 5.0
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              flex: 0.5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{width: 40}}>
              <Icon size={30} name={'cart'} color={COLORS.iconSelected} />
            </View>
            <View style={{flex: 1, backgroundColor: COLORS.white}}>
              <Text
                style={{fontSize: 14, color: COLORS.black, fontWeight: 'bold'}}>
                {dataRating.count} Products Sold
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  const PriceProduct = () => {
    return (
      <View style={{marginBottom: 20}}>
        <Text style={{fontSize: 20, color: COLORS.black, fontWeight: 'bold'}}>
          ${dataDetail.price}
        </Text>
      </View>
    );
  };

  const ButtonRemoveToCart = () => {
    return (
      <TouchableOpacity
        onPress={() => removeItemFromCart(dataDetail)}
        style={{
          backgroundColor: COLORS.iconSelected,
          height: 50,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 15,
        }}>
        <Text
          style={{
            textTransform: 'uppercase',
            fontSize: 16,
            fontWeight: 'bold',
            color: COLORS.white,
          }}>
          Remove From Cart
        </Text>
      </TouchableOpacity>
    );
  };

  const ButtonAddToCart = () => {
    return (
      <TouchableOpacity
        onPress={() => addItemToCart(dataDetail)}
        style={{
          backgroundColor: COLORS.iconSelected,
          height: 50,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 15,
        }}>
        <Text
          style={{
            textTransform: 'uppercase',
            fontSize: 16,
            fontWeight: 'bold',
            color: COLORS.white,
          }}>
          Add To Cart
        </Text>
      </TouchableOpacity>
    );
  };

  const Body = () => {
    return (
      <View style={{padding: 20}}>
        <TagProduct />
        <TitleProduct />
        <PriceProduct />
        {cart.some(value => value.id == dataDetail.id) ? (
          <ButtonRemoveToCart />
        ) : (
          <ButtonAddToCart />
        )}
      </View>
    );
  };

  return (
    <View style={{backgroundColor: COLORS.white, flex: 1}}>
      <Header />
      <Body />
    </View>
  );
};

export default DetailProduct;

const styles = StyleSheet.create({});
