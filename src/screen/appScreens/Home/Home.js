import auth from '@react-native-firebase/auth';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import COLORS from '../../../constant/COLORS';

const Home = ({navigation}) => {
  const [data, setData] = useState([]);
  const user = auth().currentUser;

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      console.log('MULAI DATA');
      const response = await fetch(
        'https://fakestoreapi.com/products?limit=10',
      );
      const json = await response.json();
      console.log(json);
      setData(json);
    } catch (error) {
      console.error(error);
    }
  };

  const Header = () => {
    return (
      <View style={{height: '10%', backgroundColor: '#FEB000'}}>
        <View
          style={{
            flexDirection: 'row',
            paddingTop: 15,
            paddingHorizontal: 20,
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
          }}>
          <View style={{flex: 5, marginRight: 10}}>
            <TextInput
              style={{
                padding: 10,
                paddingHorizontal: 40,
                height: 35,
                width: '100%',
                backgroundColor: COLORS.white,
                borderRadius: 2,
              }}
              onChangeText={text => console.log(text)}
              value={{}}
              placeholder={'Sepatu Pria'}
              placeholderTextColor={COLORS.iconSelected}
            />
            <View
              style={{
                position: 'absolute',
                top: 5,
                left: 10,
              }}>
              <Icon size={20} color={COLORS.black} name={'search-outline'} />
            </View>
            <View
              style={{
                position: 'absolute',
                top: 5,
                right: 10,
              }}>
              <Icon size={20} color={COLORS.black} name={'camera-outline'} />
            </View>
          </View>
          <View
            style={{
              flex: 0.5,
              marginHorizontal: 10,
            }}>
            <Icon size={30} color={COLORS.white} name={'cart-outline'} />
          </View>
          <View
            style={{
              flex: 0.5,
              marginLeft: 10,
            }}>
            <Icon
              size={30}
              color={COLORS.white}
              name={'chatbubble-ellipses-outline'}
            />
          </View>
        </View>
      </View>
    );
  };

  const Banner = () => {
    return (
      <Image
        source={require('../../../assets/banner/banner3.jpg')}
        resizeMode="cover"
        style={{
          width: '100%',
          height: '25%',
        }}></Image>
    );
  };

  const ItemProduct = ({item}) => (
    <TouchableOpacity
      onPress={() => navigation.navigate('DetailProduct', {data: item})}
      style={{
        height: 200,
        width: '40%',
        marginHorizontal: 20,
        marginVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderRadius: 10,
        padding: 20,
        shadowColor: '#000',
        elevation: 3,
      }}>
      <Image
        source={{
          uri: item.image,
        }}
        style={{width: 100, height: 100}}
      />
      <View style={{marginTop: 10}}>
        <Text style={{fontSize: 16, fontWeight: 'bold', color: COLORS.black}}>
          {item.title.substring(0, 10)}
        </Text>
        <Text
          style={{fontSize: 14, fontWeight: '500', color: COLORS.lightText}}>
          ${item.price}
        </Text>
      </View>
    </TouchableOpacity>
  );

  const Product = () => {
    return (
      <View style={{flex: 1, backgroundColor: COLORS.white, padding: 20}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={{flex: 2}}>
            <Text
              style={{fontSize: 24, color: COLORS.black, fontWeight: '500'}}>
              Recommendation
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-end',
            }}>
            <Text
              style={{
                fontSize: 16,
                color: COLORS.iconSelected,
                fontWeight: '500',
              }}>
              See All
            </Text>
          </View>
        </View>
        <FlatList
          data={data}
          renderItem={({item}) => <ItemProduct item={item} />}
          keyExtractor={item => item.id}
          numColumns={2}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  };

  return (
    <View style={{backgroundColor: COLORS.white, flex: 1}}>
      <StatusBar
        backgroundColor={COLORS.bgLineGradOne}
        barStyle={'dark-content'}
      />
      <Header />
      <Banner />
      <Product />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({});
