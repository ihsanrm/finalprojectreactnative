import React from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import COLORS from '../../../constant/COLORS';
import {
  decreamentQuantity,
  incrementQuantity,
  removeFromCart,
} from '../../../context/CartReducer';

const Cart = ({navigation}) => {
  const cart = useSelector(state => state.cart.cart);
  console.log('ini di halaman cart');
  console.log(cart);
  const dispatch = useDispatch();

  const HandleIncreaseQuantity = item => {
    dispatch(incrementQuantity(item));
  };

  const HandleDecreaseQuantity = item => {
    dispatch(decreamentQuantity(item));
  };

  const removeItemFromCart = item => {
    dispatch(removeFromCart(item));
  };

  const NavbarContainer = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          alignSelf: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            backgroundColor: COLORS.white,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,

            elevation: 5,
            width: 40,
            height: 40,
            borderRadius: 10,
            marginLeft: 10,
            marginTop: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon size={30} color={COLORS.black} name={'chevron-back'} />
        </TouchableOpacity>
        <View
          style={{
            flex: 1.5,
            alignItems: 'center',
            justifyContent: 'center',
            height: 40,
          }}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              color: COLORS.black,
            }}>
            Order Details
          </Text>
        </View>
      </View>
    );
  };

  const ItemCart = ({data}) => (
    <View>
      <View
        style={{
          flexDirection: 'row',
          marginVertical: 20,
          backgroundColor: COLORS.white,
        }}>
        <Image
          source={{uri: data.image}}
          style={{height: 100, width: 100, resizeMode: 'contain'}}
        />
        <View style={{marginLeft: 20}}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              color: COLORS.black,
              lineHeight: 25,
            }}>
            {data.title ? data.title.substring(0, 100) : '-'}
          </Text>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              color: COLORS.lightText,
            }}>
            ${data.price}
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => HandleDecreaseQuantity(data)}
                style={{width: 50}}>
                <Icon
                  size={30}
                  color={COLORS.iconSelected}
                  name={'remove-circle'}
                />
              </TouchableOpacity>
              <View style={{flex: 0.5}}>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: COLORS.black,
                  }}>
                  {data.quantity}
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => HandleIncreaseQuantity(data)}
                style={{width: 50}}>
                <Icon
                  size={30}
                  color={COLORS.iconSelected}
                  name={'add-circle'}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => removeItemFromCart(data)}
              style={{
                width: 50,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon size={30} color={COLORS.iconSelected} name={'trash'} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );

  const Body = () => {
    return (
      <View style={{padding: 20}}>
        <Text style={{fontSize: 24, color: COLORS.black, fontWeight: 'bold'}}>
          My Cart
        </Text>
        <FlatList
          data={cart}
          renderItem={({item}) => <ItemCart data={item} />}
          keyExtractor={item => item.id}
        />
      </View>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: COLORS.white}}>
      {/* <NavbarContainer /> */}
      <Body />
    </View>
  );
};

export default Cart;

const styles = StyleSheet.create({});
