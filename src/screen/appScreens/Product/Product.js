import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import COLORS from '../../../constant/COLORS';

const Chat = ({navigation}) => {
  const [data, setData] = useState([]);
  const [dataCategoris, setDataCategories] = useState([]);
  const [selectedCategories, setSelectedCategories] = useState('');

  useEffect(() => {
    getDataCategories();
  }, [selectedCategories]);

  const getDataCategories = async () => {
    try {
      console.log('MULAI DATA');
      const response = await fetch(
        'https://fakestoreapi.com/products/categories',
      );
      const json = await response.json();
      console.log(json);
      setDataCategories(json);
      if (!selectedCategories) {
        setSelectedCategories(json[0]);
      }

      if (json) {
        getData(selectedCategories);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const getData = async category => {
    try {
      console.log('MULAI DATA');
      console.log(category);
      const response = await fetch(
        `https://fakestoreapi.com/products/category/${category}`,
      );
      const json = await response.json();
      console.log('INI DATA');
      console.log(json);
      setData(json);
    } catch (error) {
      console.error(error);
    }
  };

  const HandleIcon = title => {
    if (title === 'electronics') {
      return 'watch';
    } else if (title === 'jewelery') {
      return 'shirt';
    } else if (title === "men's clothing") {
      return 'male';
    } else if (title === "women's clothing") {
      return 'female';
    }
  };

  const ItemCategories = () => {
    return (
      <View style={{flexDirection: 'row', marginTop: 20, marginBottom: 20}}>
        {dataCategoris.map(e => (
          <TouchableOpacity
            onPress={() => setSelectedCategories(e)}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor:
                e === selectedCategories ? COLORS.iconSelected : COLORS.white,
              marginRight: 25,
              width: '20%',
              borderRadius: 10,
              height: 100,
              borderWidth: 1,
              borderColor:
                e != selectedCategories ? COLORS.iconSelected : COLORS.white,
            }}>
            <Icon
              name={HandleIcon(e)}
              size={30}
              color={
                e != selectedCategories ? COLORS.iconSelected : COLORS.white
              }
            />
            <Text
              style={{
                fontSize: 14,
                color:
                  e != selectedCategories ? COLORS.iconSelected : COLORS.white,
                fontWeight: 'bold',
              }}>
              {e}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  const ItemData = ({item}) => (
    <TouchableOpacity
      onPress={() => navigation.navigate('DetailProduct', {data: item})}
      style={{
        height: 250,
        width: '45%',
        marginRight: 20,
        borderRadius: 10,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.white,
        shadowColor: '#000',
        elevation: 3,
        borderColor: COLORS.black,
      }}>
      <Image source={{uri: item.image}} style={{height: 100, width: 100}} />
      <View style={{marginTop: 10}}>
        <Text style={{fontSize: 16, fontWeight: '500', color: COLORS.black}}>
          {item.title ? item.title.substring(0, 30) : '-'}
        </Text>
      </View>
      <View style={{marginTop: 10}}>
        <Text style={{fontSize: 16, fontWeight: '500', color: COLORS.black}}>
          ${item.price}
        </Text>
      </View>
    </TouchableOpacity>
  );

  const ListData = () => {
    return (
      <ScrollView style={{marginTop: 20}} nestedScrollEnabled={true}>
        <FlatList
          // data={[{title: 'abc'}, {title: 'def'}]}
          data={data}
          renderItem={({item}) => <ItemData item={item} />}
          keyExtractor={item => item.id}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          style={{marginBottom: 175}}
        />
      </ScrollView>
    );
  };

  const Body = () => {
    return (
      <View style={{padding: 20}}>
        <Text style={{fontSize: 24, color: COLORS.black, fontWeight: 'bold'}}>
          All Product
        </Text>
        <ItemCategories />
        <Text style={{fontSize: 24, color: COLORS.black, fontWeight: 'bold'}}>
          List {selectedCategories}
        </Text>
        <ListData />
      </View>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: COLORS.white}}>
      <Body />
    </View>
  );
};

export default Chat;

const styles = StyleSheet.create({});
