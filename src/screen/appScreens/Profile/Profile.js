import React from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import COLORS from '../../../constant/COLORS';
import {HandleSignOutFirebase} from '../../../util/util';

const Profile = () => {
  const HandleSignOut = () => {
    try {
      HandleSignOutFirebase();
      console.log('Signed Out');
    } catch (error) {
      console.log('error');
    }
  };

  const Header = () => {
    return (
      <View style={{height: '35%', backgroundColor: COLORS.white}}>
        <LinearGradient
          colors={['#FF6232', '#F84D2F']}
          style={{height: '50%'}}></LinearGradient>
        <View style={{position: 'absolute', top: 80, left: 135}}>
          <Image source={require('../../../assets/profile/profile.png')} />
        </View>
        <View
          style={{
            position: 'absolute',
            top: 20,
            right: 5,
            flexDirection: 'row',
          }}>
          <View style={{width: 50}}>
            <Icon name={'settings'} size={25} color={COLORS.white} />
          </View>
          <TouchableOpacity onPress={() => HandleSignOut()} style={{width: 50}}>
            <Icon name={'log-out'} size={25} color={COLORS.white} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            position: 'absolute',
            top: 20,
            left: 0,
            backgroundColor: COLORS.white,
            width: 100,
            height: 30,
            justifyContent: 'center',
            alignItems: 'center',
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10,
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',
              color: COLORS.iconSelected,
            }}>
            My Profile
          </Text>
        </View>
      </View>
    );
  };

  const HeaderInformation = () => {
    return (
      <View style={{alignItems: 'center', marginBottom: 20}}>
        <Text
          style={{
            fontSize: 20,
            color: COLORS.black,
            fontWeight: 'bold',
            marginBottom: 10,
          }}>
          Ihsan Rahmat Maulana
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            height: 30,
          }}>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text
              style={{
                fontSize: 16,
                color: COLORS.lightText,
                fontWeight: '500',
              }}>
              Pengikut: 50
            </Text>
          </View>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text
              style={{
                fontSize: 16,
                color: COLORS.lightText,
                fontWeight: '500',
              }}>
              Mengikuti: 50
            </Text>
          </View>
        </View>
      </View>
    );
  };

  const ItemOrder = ({data}) => (
    <TouchableOpacity
      style={{
        height: 100,
        width: 90,
        alignItems: 'center',
      }}>
      <Icon name={data.icons} size={35} color={COLORS.iconSelected} />
      <Text style={{}}>{data.title}</Text>
    </TouchableOpacity>
  );

  const MyOrder = () => {
    return (
      <View
        style={{
          paddingHorizontal: 20,
          borderTopColor: '#E5E5E5',
          borderBottomColor: '#E5E5E5',
          borderBottomWidth: 10,
          borderTopWidth: 10,
          paddingTop: 20,
        }}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 1}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={{width: 30}}>
                <Icon
                  name={'document-text'}
                  size={25}
                  color={COLORS.iconSelected}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: '500',
                    color: COLORS.black,
                  }}>
                  My Orders
                </Text>
              </View>
            </View>
          </View>
          <TouchableOpacity
            style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
            <Text
              style={{
                fontSize: 14,
                fontWeight: 'bold',
                color: COLORS.iconSelected,
              }}>
              History Orders
            </Text>
          </TouchableOpacity>
        </View>
        <FlatList
          data={[
            {
              title: 'Belum Dibayar',
              icons: 'wallet',
            },
            {
              title: 'Dikemas',
              icons: 'car',
            },
            {
              title: 'Dikirim',
              icons: 'gift',
            },
            {
              title: 'Beri Penilaian',
              icons: 'star-half',
            },
          ]}
          renderItem={({item}) => <ItemOrder data={item} />}
          keyExtractor={item => item.id}
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{marginTop: 20}}
        />
      </View>
    );
  };

  const ListMenu = () => {
    return (
      <View style={{paddingLeft: 20, paddingVertical: 20}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 20,
          }}>
          <View style={{width: 50}}>
            <Icon name={'settings'} size={30} color={COLORS.iconSelected} />
          </View>
          <View style={{flex: 1}}>
            <Text
              style={{fontSize: 18, fontWeight: '500', color: COLORS.black}}>
              Setting
            </Text>
          </View>
          <View style={{width: 50}}>
            <Icon
              name={'chevron-forward'}
              size={30}
              color={COLORS.iconSelected}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={() => HandleSignOut()}
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 20,
          }}>
          <View style={{width: 50}}>
            <Icon name={'log-out'} size={30} color={COLORS.iconSelected} />
          </View>
          <View style={{flex: 1}}>
            <Text
              style={{fontSize: 18, fontWeight: '500', color: COLORS.black}}>
              Sign Out
            </Text>
          </View>
          <View style={{width: 50}}>
            <Icon
              name={'chevron-forward'}
              size={30}
              color={COLORS.iconSelected}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: COLORS.white}}>
      <Header />
      <HeaderInformation />
      <MyOrder />
      <ListMenu />
    </View>
  );
};

export default Profile;
