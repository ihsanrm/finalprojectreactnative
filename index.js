/**
 * @format
 */

import {AUTH_TOKEN} from '@env';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

console.log(AUTH_TOKEN);

AppRegistry.registerComponent(appName, () => App);
