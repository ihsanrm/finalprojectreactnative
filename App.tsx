import React from 'react';
import {LogBox, StyleSheet} from 'react-native';
import {Provider} from 'react-redux';
import Store from './src/context/Store';
import Router from './src/router';

const App = () => {
  LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
  LogBox.ignoreAllLogs(); //Ignore all log notifications
  return (
    <Provider store={Store}>
      <Router />
    </Provider>
  );
};

export default App;

const styles = StyleSheet.create({});
